/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {Platform,StyleSheet,Text,View,Image} from 'react-native';

import AlbumHeader from './src/components/AlbumHeader'
import AlbumList from './src/components/AlbumList'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={{flex:1  }}>
        <AlbumHeader headerName={'Albums'}/>
        <AlbumList/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
});
