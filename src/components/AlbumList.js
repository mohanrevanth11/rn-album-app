import React, {Component} from 'react';
import ReactNative, {Text, ScrollView, StyleSheet} from 'react-native';
import AlbumDetail from './AlbumDetail'

export default class AlbumList extends Component{

    state = {albums: []};

    componentWillMount(){
        fetch('https://rallycoding.herokuapp.com/api/music_albums')
            .then(response => response.json())
            .then(data => this.setState({albums: data}));
    }
    renderAlbums() {        
        return this.state.albums.map(album => <AlbumDetail detailProp={album} key={album.title}/>);
    }
    render() {
        return (
            <ScrollView>
                {this.renderAlbums()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({


}
);