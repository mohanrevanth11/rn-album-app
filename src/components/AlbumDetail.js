import React, {Component} from 'react';
import ReactNative, {Text, View, StyleSheet, Image, TouchableOpacity, Linking} from 'react-native';
import Card from './Card';
import CardSection from './CardSection';

export default class AlbumDetail extends Component{
    render() {
        return(
            <TouchableOpacity onPress = {() => Linking.openURL(this.props.detailProp.url)}>
                <Card>
                    <CardSection>
                        <View style={styles.thumbnailContainer}>
                            <Image 
                                style = {styles.thumbnail}
                                source={{uri: this.props.detailProp.thumbnail_image}}/>
                        </View>
                        <View style={styles.headerText}>
                            <Text style= {styles.titleText}> {this.props.detailProp.title}</Text>
                            <Text> {this.props.detailProp.artist}</Text>
                        </View>                    
                    </CardSection>
                    <CardSection>
                        <Image source = {{uri:this.props.detailProp.image}}
                                style = {styles.image} />
                    </CardSection>
                </Card>
            </TouchableOpacity>
        )
    }
}


//stylesheet

const styles = StyleSheet.create({
    headerText:{
        flexDirection:'column',
        justifyContent:'space-around'
    },
    thumbnail:{
        height: 50,
        width: 50
    },
    thumbnailContainer:{
        marginLeft: 8,
        marginRight: 8,
        justifyContent: 'center',
        alignContent:'center'
    },
    titleText:{
        fontSize: 18,
        fontWeight: 'bold'
    },
    image:{
        height: 300,
        flex: 1,
        //width: null
    }
});