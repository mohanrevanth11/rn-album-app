import React, {Component} from 'react';
import ReactNative, {Text, View, StyleSheet, Image} from 'react-native';

const CardSection = (props) =>{
    return(
        <View style={styles.containerStyle}>
            {props.children}
        </View>
    );
};

export default CardSection;

const styles = StyleSheet.create({
    containerStyle:{
        padding: 8,
        backgroundColor: '#FFF',
        justifyContent: 'flex-start',
        flexDirection:'row',
        position:'relative'
    }

}
)